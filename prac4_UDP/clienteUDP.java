/** Programa que recibe datagramas UDP en el puerto 12345 
    y que muestra por pantalla lo que va recibiendo
**/

import java.net.*;
import java.io.*;
import java.util.Scanner;

class broadcastUDPeJ44
{

  public static void main( String args[]) throws IOException
  {
    
    // Creacion de un socket UDP que escucha en el puerto 12345
    DatagramSocket ds= new DatagramSocket(12345) ;
    String str="";

    do
    {
	
	   str=recibeLinea(ds);
        	   	     	
    }while(!str.startsWith("//COMIENZO"));
    
    do
    {
	    System.out.println(""+str);
	    str=recibeLinea(ds);
        	   	     	
    }while(!str.startsWith("//COMIENZO"));
   



  } //fin del main


  public static String  recibeLinea(DatagramSocket ds) throws IOException
  {
      
      //Preparamos  un datagrama UDP para recibir
      DatagramPacket p=new DatagramPacket (new byte[512],512);

      ds.receive(p); // se bloquea hasta que recibe un datagrama

      // pasamos el paquete UDP que viene en array de bytes a String
      String resp= new String(p.getData(),0,p.getLength());
    	
      return resp;
  }

} // fin de la clase



