import java.net.*;
import java.io.*;
import java.util.*;
// Cliente TCP. 
// Envia al servidor un fichero. Primera linea: nombre + linea en blanco + contenido en binario 

class ClienteFicheroTCP {
	public static void main(String args[]) {
		int bytes;
		byte[] buffer = new byte[1024];
		int port;
		String nombre = null;
		FileInputStream fich = null;

		try {
			Socket s = new Socket("localhost",7000);
			try{
			    nombre = args[0];
			
                            fich = new FileInputStream(nombre);
			    PrintWriter esc=new PrintWriter(s.getOutputStream());
			    Scanner lee = new Scanner(s.getInputStream());
			    esc.print(nombre + "\r\n");esc.flush();   // nombre + linea en blanco
			    System.out.println(nombre);
			    try{
				port = Integer.parseInt(lee.nextLine());
				System.out.println("port= "+port);
				Socket s2 = new Socket("localhost",port);
				while ((bytes = fich.read(buffer))!= -1){
				//    System.out.println(new String(buffer, 0 , bytes));
				    s2.getOutputStream().write(buffer, 0, bytes);
				//    System.out.println("bytes = "+ bytes);
			     }
			s.getOutputStream().flush();
			}
			catch(NumberFormatException e) {
				System.out.println("Error numero de puerto enviado por el servidor incorrecto");
				System.exit(-1);
			}
			} // try nombre
			catch(ArrayIndexOutOfBoundsException e) {System.out.println("Error: falta el nombre del fichero. Uso: ClienteFicheroTCP nombre_fichero");
			      System.exit(0);
			}
			
			fich.close(); 
		  	s.close();
			}
			catch (Exception e) {
					System.err.println(e);
			}
	}
}