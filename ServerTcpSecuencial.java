/**  Ejercicio Opcional del tema 3  Eduardo Pablo Novella Lorente
     El servidor esperará las conexiones de los clientes en el puerto 7000.
     Al conectarse un cliente, enviará al servidor una o más palabras concatenadas que representan el nombre de un fichero (por ejemplo, prueba.pdf), acabadas por la secuencia de fín de línea “\r\n”. El cliente transmitirá posteriormente este fichero al servidor por otra conexión distinta.
     El servidor concatenará el nombre recibido con la cadena “./v2_”. Se trata de una precaución para que no tengáis problemas con el fichero, si ejecutáis el cliente y el servidor en el mismo directorio,.
     Después de recibir el nombre, el servidor creará un nuevo socket servidor al que el cliente debe conectarse y transmitirle el fichero. El puerto asociado a este socket será aleatorio (elegido por el sistema operativo). Para que el cliente pueda saber el puerto asociado a este nuevo socket, el servidor se lo enviará como una cadena de texto, por la conexión que ambos tienen establecida.
     Tras recibir el número de puerto donde escucha el servidor, el cliente se conectará a ese puerto, le enviará al servidor el fichero y cerrará la conexión. Se supone que puede tratarse de un fichero binario. El servidor almacenará la información recibida en un fichero. El nombre de este fichero es el indicado por el cliente en la conexión inicial establecida al puerto 7000. 
     La escritura del fichero puede realizarse fácilmente empleando la clase de java FileOuputStream.      Se proporciona como material adicional para poder realizar las pruebas el byte-code del programa cliente que envía el fichero, en el fichero “ClienteFicheroTCP.class”. Este cliente se conecta al  puerto 7000 de "localhost".

*/

import java.net.*;
import java.io.*;
import java.util.Scanner;


public class ServerTcpSecuencial
{
  
    public static void main(String args[]) throws IOException
    {
      
      try
      {
	  //creamos el serversocket escuchando en el puerto 7000
	  ServerSocket ss=new ServerSocket(7000);


	  while(true)
	  {
	    Socket s = ss.accept();    // esperamos algun cliente
	      
	    // preparamos la entrada pa recojer datos del cliente
	    Scanner in= new Scanner (s.getInputStream());
	    PrintWriter out=new PrintWriter(s.getOutputStream(),true);

	    //concatenamos 
	    String str=("./v2_"+in.next()).trim();	    

	    //Creamos otro serversocket para que el cliente transfiera el fichero
	    // ademas dejamos al operativo que elija el puerto poniendo (0)
	    //mediante un string mandaremos el numero de puerto para 
	    // que nos envie el fichero
	    ServerSocket sst= new ServerSocket(0); 	    
	    
	    //enviamos printeado el puerto,sera un string 
	    String port=sst.getLocalPort()+""; // int2str
	    out.println(port);

	    ss.close(); //cerramos el socketserver
	    s.close(); //cerramos el socketclient

	    Socket s2=sst.accept();
	    Scanner ini= new Scanner (s2.getInputStream());

	    // creamos fichero con el nombre que nos dio el client	 
	    //FileOutputStream fich=new FileOutputStream (new File(str));
	    //byte[] buffer=new byte[1024];
	    PrintWriter fich=new PrintWriter(new File(str));

            while (ini.hasNext() )
	    {
		fich.println(ini.nextLine());		      
	    }
	   
	    fich.close();
	  } //fin dl while


      }//fin dl try
      catch(FileNotFoundException e)
      {	
	  System.out.println("Error con el fichero,puede ser que no exista");
      }
      catch(Exception e){
	
      }//fin del catch
      
    }//fin del main

}// fin de la clase